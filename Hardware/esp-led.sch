EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP-12F U1
U 1 1 60CC6E06
P 5050 3100
F 0 "U1" H 5050 4081 50  0000 C CNN
F 1 "ESP-12F" H 5050 3990 50  0000 C CNN
F 2 "RF_Module:ESP-12E" H 5050 3100 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 4700 3200 50  0001 C CNN
	1    5050 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60CC8A2B
P 4000 2000
F 0 "R1" H 4070 2046 50  0000 L CNN
F 1 "10k" H 4070 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3930 2000 50  0001 C CNN
F 3 "~" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60CCAB38
P 4250 2000
F 0 "R2" H 4320 2046 50  0000 L CNN
F 1 "10k" H 4320 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4180 2000 50  0001 C CNN
F 3 "~" H 4250 2000 50  0001 C CNN
	1    4250 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60CCAF37
P 6400 1950
F 0 "R3" H 6470 1996 50  0000 L CNN
F 1 "10k" H 6470 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6330 1950 50  0001 C CNN
F 3 "~" H 6400 1950 50  0001 C CNN
	1    6400 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60CCB23C
P 5850 4000
F 0 "R4" H 5920 4046 50  0000 L CNN
F 1 "10k" H 5920 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5780 4000 50  0001 C CNN
F 3 "~" H 5850 4000 50  0001 C CNN
	1    5850 4000
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM1117-3.3 U2
U 1 1 60CCD93B
P 1900 1700
F 0 "U2" H 1900 1942 50  0000 C CNN
F 1 "LM1117-3.3" H 1900 1851 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 1900 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 1900 1700 50  0001 C CNN
	1    1900 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 60CCEE87
P 1300 2000
F 0 "C1" H 1418 2046 50  0000 L CNN
F 1 "10µF" H 1418 1955 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 1338 1850 50  0001 C CNN
F 3 "~" H 1300 2000 50  0001 C CNN
	1    1300 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 60CCF9D2
P 2550 2000
F 0 "C2" H 2668 2046 50  0000 L CNN
F 1 "10µF" H 2668 1955 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 2588 1850 50  0001 C CNN
F 3 "~" H 2550 2000 50  0001 C CNN
	1    2550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1700 1300 1700
Wire Wire Line
	1300 1700 1300 1850
Wire Wire Line
	2200 1700 2550 1700
Wire Wire Line
	2550 1700 2550 1850
$Comp
L power:GND #PWR0101
U 1 1 60CD1BEE
P 750 4400
F 0 "#PWR0101" H 750 4150 50  0001 C CNN
F 1 "GND" H 755 4227 50  0000 C CNN
F 2 "" H 750 4400 50  0001 C CNN
F 3 "" H 750 4400 50  0001 C CNN
	1    750  4400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0102
U 1 1 60CD2200
P 750 1250
F 0 "#PWR0102" H 750 1100 50  0001 C CNN
F 1 "+12V" H 765 1423 50  0000 C CNN
F 2 "" H 750 1250 50  0001 C CNN
F 3 "" H 750 1250 50  0001 C CNN
	1    750  1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2150 1900 2150
Wire Wire Line
	1900 2000 1900 2150
Connection ~ 1900 2150
Wire Wire Line
	1900 2150 2550 2150
Wire Wire Line
	750  4400 1900 4400
Wire Wire Line
	1900 2150 1900 4400
Wire Wire Line
	1900 4400 3900 4400
Wire Wire Line
	5050 4400 5050 3800
Connection ~ 1900 4400
Wire Wire Line
	5050 2300 5050 2250
Wire Wire Line
	5050 1700 4250 1700
Connection ~ 2550 1700
$Comp
L Device:C C3
U 1 1 60CD87FB
P 3900 3950
F 0 "C3" H 4015 3996 50  0000 L CNN
F 1 "100nF" H 4015 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3938 3800 50  0001 C CNN
F 3 "~" H 3900 3950 50  0001 C CNN
	1    3900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2250 3900 2250
Wire Wire Line
	3900 2250 3900 3800
Connection ~ 5050 2250
Wire Wire Line
	5050 2250 5050 1700
Wire Wire Line
	3900 4100 3900 4400
Connection ~ 3900 4400
Wire Wire Line
	3900 4400 5050 4400
Wire Wire Line
	4450 2500 4250 2500
Wire Wire Line
	4250 1850 4250 1700
Connection ~ 4250 1700
Wire Wire Line
	4250 1700 4000 1700
$Comp
L Transistor_FET:IRLB8721PBF Q1_blue1
U 1 1 60CDB416
P 8700 2450
F 0 "Q1_blue1" H 8904 2496 50  0000 L CNN
F 1 "IRLR8726PBF" H 8904 2405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin2" H 8950 2375 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlb8721pbf.pdf?fileId=5546d462533600a40153566056732591" H 8700 2450 50  0001 L CNN
	1    8700 2450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRLB8721PBF Q2_red1
U 1 1 60CDDA75
P 8700 3000
F 0 "Q2_red1" H 8904 3046 50  0000 L CNN
F 1 "IRLR8726PBF" H 8904 2955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin2" H 8950 2925 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlb8721pbf.pdf?fileId=5546d462533600a40153566056732591" H 8700 3000 50  0001 L CNN
	1    8700 3000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRLB8721PBF Q3_green1
U 1 1 60CDE33C
P 8700 3550
F 0 "Q3_green1" H 8904 3596 50  0000 L CNN
F 1 "IRLR8726PBF" H 8904 3505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin2" H 8950 3475 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlb8721pbf.pdf?fileId=5546d462533600a40153566056732591" H 8700 3550 50  0001 L CNN
	1    8700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4400 5850 4400
Wire Wire Line
	5850 4400 5850 4150
Connection ~ 5050 4400
Wire Wire Line
	5850 3850 5850 3400
Wire Wire Line
	5850 3400 5650 3400
$Comp
L Switch:SW_Push SW1
U 1 1 60CE3704
P 6400 4000
F 0 "SW1" V 6446 3952 50  0000 R CNN
F 1 "Flash" V 6355 3952 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_CK_KMR2" H 6400 4200 50  0001 C CNN
F 3 "~" H 6400 4200 50  0001 C CNN
	1    6400 4000
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 60CE55A1
P 6850 4000
F 0 "SW2" V 6896 3952 50  0000 R CNN
F 1 "Reset" V 6805 3952 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_CK_KMR2" H 6850 4200 50  0001 C CNN
F 3 "~" H 6850 4200 50  0001 C CNN
	1    6850 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 2500 4250 2150
Wire Wire Line
	6850 3800 6850 2150
Wire Wire Line
	6850 2150 4250 2150
Connection ~ 4250 2150
Wire Wire Line
	6400 3800 6400 2500
Wire Wire Line
	6400 2500 5650 2500
Wire Wire Line
	6400 2500 6400 2100
Connection ~ 6400 2500
Wire Wire Line
	6400 1800 6400 1700
Wire Wire Line
	6400 1700 5050 1700
Connection ~ 5050 1700
Wire Wire Line
	5850 4400 6400 4400
Wire Wire Line
	6850 4400 6850 4200
Connection ~ 5850 4400
Wire Wire Line
	6400 4200 6400 4400
Connection ~ 6400 4400
Wire Wire Line
	6400 4400 6850 4400
Wire Wire Line
	8800 2650 9600 2650
Wire Wire Line
	9600 2650 9600 3200
Wire Wire Line
	9600 4400 7800 4400
Connection ~ 6850 4400
Wire Wire Line
	8800 3750 9600 3750
Connection ~ 9600 3750
Wire Wire Line
	9600 3750 9600 4400
Wire Wire Line
	8800 3200 9600 3200
Connection ~ 9600 3200
Wire Wire Line
	9600 3200 9600 3750
Wire Wire Line
	8500 2450 6950 2450
Wire Wire Line
	6950 2450 6950 3100
Wire Wire Line
	6950 3100 5650 3100
Wire Wire Line
	8500 3000 7100 3000
Wire Wire Line
	7100 3000 7100 3300
Wire Wire Line
	7100 3300 5650 3300
Wire Wire Line
	8500 3550 7000 3550
Wire Wire Line
	7000 3550 7000 3000
Wire Wire Line
	7000 3000 5650 3000
Wire Wire Line
	4000 1700 4000 1850
Connection ~ 4000 1700
Wire Wire Line
	4000 1700 2550 1700
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 60CF1F5E
P 10350 2400
F 0 "J2" H 10378 2376 50  0000 L CNN
F 1 "+12 G R B" H 10378 2285 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 10350 2400 50  0001 C CNN
F 3 "~" H 10350 2400 50  0001 C CNN
	1    10350 2400
	1    0    0    1   
$EndComp
Wire Wire Line
	10150 2300 9900 2300
Wire Wire Line
	9900 2300 9900 2800
Wire Wire Line
	9900 2800 8800 2800
Wire Wire Line
	10150 2200 8800 2200
Wire Wire Line
	8800 2200 8800 2250
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 60CF7346
P 550 3100
F 0 "J1" H 442 3285 50  0000 C CNN
F 1 "12V" H 442 3194 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 550 3100 50  0001 C CNN
F 3 "~" H 550 3100 50  0001 C CNN
	1    550  3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	750  3200 750  4400
Connection ~ 750  4400
Wire Wire Line
	4450 2700 4000 2700
Wire Wire Line
	4000 2700 4000 2150
$Comp
L Device:D D1
U 1 1 60D07A7F
P 1050 1700
F 0 "D1" H 1050 1483 50  0000 C CNN
F 1 "D" H 1050 1574 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 1050 1700 50  0001 C CNN
F 3 "~" H 1050 1700 50  0001 C CNN
	1    1050 1700
	-1   0    0    1   
$EndComp
Wire Wire Line
	750  1250 750  1350
Wire Wire Line
	1300 1700 1200 1700
Connection ~ 1300 1700
Wire Wire Line
	900  1700 750  1700
Connection ~ 750  1700
Wire Wire Line
	750  1700 750  3100
Text Label 4850 1700 0    50   ~ 0
3v3
Text Label 750  1650 0    50   ~ 0
12v
Text Label 7950 2450 0    50   ~ 0
bluePwmLL
Text Label 8000 3000 0    50   ~ 0
redPwmLL
Text Label 7900 3550 0    50   ~ 0
greenPwmLL
Text Label 9550 2200 0    50   ~ 0
bluePwmDrive
Text Label 8950 2800 0    50   ~ 0
redPwmDrive
Text Label 9000 3350 0    50   ~ 0
greenPwmDrive
Text Label 1200 4400 0    50   ~ 0
GND
Text Label 2950 1700 0    50   ~ 0
3v3
Text Label 1250 1700 0    50   ~ 0
12v-aftDio
Text Label 6400 2750 0    50   ~ 0
nFlash
Text Label 6850 2250 0    50   ~ 0
nReset
Text Label 4100 2700 0    50   ~ 0
EN
Connection ~ 750  1350
Wire Wire Line
	750  1350 750  1700
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 60CE63FB
P 8000 4000
F 0 "J3" H 7972 3932 50  0000 R CNN
F 1 "Serial" H 7972 4023 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8000 4000 50  0001 C CNN
F 3 "~" H 8000 4000 50  0001 C CNN
	1    8000 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	7800 4100 7800 4400
Connection ~ 7800 4400
Wire Wire Line
	7800 4400 6850 4400
Wire Wire Line
	7650 2600 5650 2600
Wire Wire Line
	7800 3900 7800 2800
Wire Wire Line
	5650 2800 7800 2800
Wire Wire Line
	7650 2600 7650 4000
Wire Wire Line
	7650 4000 7800 4000
Wire Wire Line
	8800 3350 10050 3350
Wire Wire Line
	10050 3350 10050 2400
Wire Wire Line
	10050 2400 10150 2400
Wire Wire Line
	10150 2500 9450 2500
Wire Wire Line
	9450 2500 9450 1350
Wire Wire Line
	9450 1350 750  1350
Text Label 7650 3700 0    50   ~ 0
TX
Text Label 7800 3700 0    50   ~ 0
RX
Text Notes 7000 7100 0    50   ~ 0
Source: https://chrisklinger.de/2018/05/arduino-esp8266-wifi-steuerung-fuer-einen-rgb-led-strip/
$EndSCHEMATC
